bind \eh 'peco --history (commandline)'
bind \et 'peco --terminate (commandline); commandline ""'
bind \eo 'peco --open (commandline); commandline ""'
bind \ef \
'if commandline | string match -qr "\{\{.+\}\}"
    set -l pos (math (commandline | string match -r "^[^{]+" | wc -m) -1)
    commandline (commandline | string replace -r "\{\{[^}]+\}\}" "")
    commandline --cursor $pos
  else; peco --commands (commandline); end
'
