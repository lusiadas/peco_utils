# Offer to uninstall dependencies
test -e $path/hooks/uninstall_dependencies.fish
or wget -qO $path/hooks/uninstall_dependencies.fish \
https://gitlab.com/snippets/1765023/raw
if test -e $path/hooks/uninstall_dependencies.fish
  source $path/hooks/uninstall_dependencies.fish
  uninstall curl file mlocate peco sensible-utils tldr xdg-utils
end

# Remove autoloaded functions
functions -e peco_commands
functions -e dependencies
functions -e uninstall
