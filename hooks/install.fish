# Install feedback plugin
if type -q omf
  if not omf list | string match -qe feedback
    if type -q fisher
      fisher list | string match -qe feedback
      or omf install https://gitlab.com/lusiadas/feedback
    else
      omf install https://gitlab.com/lusiadas/feedback
    end
  end
else
  fisher list | string match -qe feedback
  or fisher install gitlab.com/lusiadas/feedback
end

# Install uninstall function
wget -qO $path/hooks/uninstall_dependencies.fish \
https://gitlab.com/snippets/1765023/raw

# Install dependency function
wget -qO $path/hooks/dependencies.fish \
https://gitlab.com/snippets/1762340/raw
and source $path/hooks/dependencies.fish

# Install check_peco function
wget -qO $path/auxiliary_functions/check_peco.fish \
https://gitlab.com/snippets/1771685/raw
and source $path/auxiliary_functions/check_peco.fish

# Install peco_utils dependencies
check_peco
check_open_dependencies
check_commands_dependencies
and update_command_list
