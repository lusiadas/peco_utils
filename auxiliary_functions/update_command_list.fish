function update_command_list -V path

  # Determine kernel
  set -l kernel
  if string match -q Linux (uname -s); set kernel linux
  else if string match -q Darwin (uname -s); set kernel osx
  else if string match -qr '(CYGWIN|WINDOWS)' (uname -s); set kernel windows
  else if string match -q SunOS (uname -s); set kernel sunos
  end

  # Query command list
  test -d "$HOME/.tldr/cache/pages"; or tldr update
  test -n "$kernel"
  and set -l pages (realpath $HOME/.tldr/cache/pages/{common,$kernel}/*)
  or set -l pages (realpath $HOME/.tldr/cache/pages/common/*)

  # See if modifications have been made
  set -l command_list "$path/command_list"
  if test -e $command_list
    if string match -q (tail -1 $command_list) \
    (ls -lt $pages | grep -oP '\S+\s+\d+\s+\d{2}:\d{2}' -m 1)
      win "Command list is up to date"
      return 0
    end
  end

  # Update command list
  wrn "Updating command list. This might take a minute."
  echo -n > $command_list
  for page in $pages
    set -l specifier (grep -hoP '(?<=# ).*' $page)
    set -l description (grep -hoP '(?<=> ).*' -m 1 $page)
    echo "$specifier ($description)" >> $command_list
    set -l commands (grep -hoP '(?<=^`).*(?=`$)' $page)
    set -l descriptions (grep -hoP '(?<=^- ).*(?=:$)' $page)
    for i in (seq (count $commands))
      echo "$commands[$i] ($descriptions[$i])" >> $command_list
    end
  end
  ls -lt $pages | grep -oP '\S+\s+\d+\s+\d{2}:\d{2}' -m 1  >> $command_list
  win "Command list succesfully updated"
end
