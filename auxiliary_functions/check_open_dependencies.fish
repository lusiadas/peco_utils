function check_open_dependencies -d 'Check dependencies for the --open option'
  set -l failed

  # Check for dependency function
  set -l dependencies (dirname (status -f))/../hooks/dependencies.fish
  if not test -e $dependencies
    wget -qO $dependencies https://gitlab.com/snippets/1762340/
    or return 1
  end
  source $dependencies

  # Check for file
  if not type -qf file
    dependency install file
    or set failed true
  end

  # Check for mlocate
  if not type -qf locate
    dependency install mlocate
    or set failed true
  end
  if not test -s /var/lib/mlocate/mlocate.db -o \
  -s /data/data/com.termux/files/usr/var/mlocate/mlocate.db
    wrn "There's no index of files to locate from"
    read -P "Generate database index? This should take from 2 to 5 minutes [y/n]: "
    string match -qr '(?i)^y$' $opt
    and sudo updatedb; or set failed true
  end

  # Check for sensible-utils
  if not type -qf sensible-editor
    dependency install sensible-utils
    or set failed true
  end

  # Check for xdg-open
  if not type -qf xdg-open
    dependency xdg-utils
    or set failed true
  end
  functions -e dependency
  test -z "$failed"
end
