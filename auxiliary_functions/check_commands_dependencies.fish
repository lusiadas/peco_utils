function check_commands_dependencies -d 'Check dependencies fot the --command option'

  # Check for dependency function
  set -l dependencies (dirname (status -f))/../hooks/dependencies.fish
  if not test -e $dependencies
    wget -qO $dependencies https://gitlab.com/snippets/1762340/
    or return 1
  end
  source $dependencies

  # Install tldr
  if not type -qf curl
    dependency install curl; or return 1
  end
  if not type -qf npm
    curl -L https://www.npmjs.com/install.sh | sh; or return 1
  end
  if not type -qf tldr
    sudo npm install -g tldr
  end
  functions -e dependency
end

