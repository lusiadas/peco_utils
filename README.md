<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

# peco_utils

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

## Description

A wrapper script for [peco](https://github.com/peco/peco), the simplistic interactive filtering tool, with additional options and fully fledged completions.

## Additional options

```
-y, --history         search for commands in your history

-t, --terminate       search for processes to terminate

-o, --open            search for files to open with their default applications

-c, --commands				search for examples of command line usage. It also has two suboptions:

-u, --update					Update the command list from where to draw examples from

-e, --editor					Modify, or create, a page with usage examples of a given command.
```

## Install

Either with omf
```fish
omf install peco_utils
```
or using [fisherman](https://github.com/fisherman/fisherman)
```fish
fisher gitlab.com/lusiadas/peco_utils
```

## Dependencies

Installed using the default package manager:

`curl mlocate file sensible-utils xdg-open`

Installed using npm:

`tldr`

Installed using omf or fisher:

`feedback`

If you don't have those installed already, upon installing `peco_utils` you'll be prompted to install them as well.

## Optional configurations

### Keybindings

By default, this script binds Alt+H, Alt+T, Alt+O and Alt+F to the additional fish options. Like so:

```
bind \eh 'peco --history (commandline)'
bind \et 'peco --terminate (commandline); commandline ""'
bind \eo 'peco --open (commandline); commandline ""'
bind \ef 'peco_commands'
```

Alt+F has a double function: At first, it passes the content of the current command line to `peco --comands`. After selecting an option from `peco --comands`, it jumps between spaces to be filled in with arguments.

To change these keybindings, see `man bind`, and modify the file `key_bindings.fish`.

### Peco configuration

This is the config file that I use for `peco`:

```
{
	"Prompt":">",
	"Layout":"bottom-up",
	"Action": {
		"selectAllAndFinish": [
			"peco.SelectAll",
			"peco.Finish"
		]
	},
	"Keymap": {
		"C-Enter":"selectAllAndFinish",
		"Home":"peco.BeginningOfLine",
		"End":"peco.EndOfLine",
		"Pgup":"peco.ScrollPageUp",
		"Pgdn":"peco.ScrollPageDown",
		"ArrowLeft":"peco.BackwardChar",
		"ArrowRight":"peco.ForwardChar"
	}
}

```

It places the search bar at the bottom of the screen, makes `Ctrl+Enter` a shortcut to accept all options presented for the query, and changes the default keybindings to bindings that are more in line with those of text editor applications. To use the configuration above, use the following command:
```
sed -nr '/^\{/,/^\}/p' $HOME/.local/share/omf/pkg/peco_utils/README.md > $HOME/.config/peco/config.json
```

---

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
