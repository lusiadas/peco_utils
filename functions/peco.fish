set -l cmd (basename (status -f) | cut -d '.' -f 1)
function $cmd -V cmd

  # Source auxiliary functions
  set -l auxiliary_functions \
  (dirname (status -f))/../auxiliary_functions/{check_open_dependencies,check_commands_dependencies,check_peco,update_command_list}.fish
  if test (count $auxiliary_functions) -ne 4
    omf update $cmd; or return 1
  end
  for function in $auxiliary_functions; source $function; end

  # Parse options
  argparse 'c/commands' 'e/edit' 'u/update' 'y/history' 't/terminate' \
  'o/open' 'h/help' -- $argv 2>/dev/null

  # Test for unknown flags
  if string match -qv -- -- "$argv[1]"
    set -l unknown_flags (string match -vr -- '^-([cytohbeu]+|(-((commands)|(history)|(terminate)|(open)|(help)|((print-)?query)|(rcfile)|(version)|(buffer-size)|(null)|(initial-((index)|(matcher)|(filter))|(prompt)|(layout)|(select((-1))|(ion-prefix)))|(on-cancel)|(exec)|(edit)|(update))))$' $argv | string match -r '^-.*' | sort -u)
    if test -n "$unknown_flags"
      if string match -qr -- '-[^-]{2,}' "$unknown_flags"
        test (count $unknown_flags) -gt 1
        and set unknown_flags 's "|'(string join -- '|", "|' $unknown_flags)'|"'
        or set unknown_flags "s |$unknown_flags|"
      else
        test (count $unknown_flags) -gt 1
        and set unknown_flags 's "|'(string join -- '|", "|' $unknown_flags)'|"'
        or set unknown_flags " |$unknown_flags|"
      end
      err "Unknown flag$unknown_flags"; return 1
    end

  # Test for invalid flag combinations
  else if test (set --names | string match -r '_flag_.$' | wc -l) -gt 1
    if test -z "$_flag_commands" -o test (count $_flag_edit $_flag_update) -ne 1
      err 'Invalid flag combination'
      reg "Use |$cmd -h| to see examples of valid syntaxes"
      echo; return 1;
    end

  # Check for dependencies
  else if not type -qf peco
    wrn "|peco| is not installed"
    if not check_peco
      functions -e (string match -r '[^/]+(?=.fish$)' $auxiliary_functions)
      return 1
    else; functions -e check_peco; end
  end

  # Query command options
  if set --query _flag_commands
    if not check_commands_dependencies
      functions -e (string match -r '[^/]+(?=.fish$)' $auxiliary_functions)
      return 1
    else; functions -e check_commands_dependencies; end
    set -l command_list (dirname (status -f))/../command_list
    if set --query _flag_update; update_command_list
    else if set --query _flag_edit
      if test -z "$_flag_edit"
        err "Flag |-e/--edit| needs a value"; return 1
      end
      set -l page (find $HOME/.tldr/cache/pages/ -type f \
      | string match -e "/$argv.md")
      if test -z "$page"
        wrn "No page found for |$argv|"
        read -P "Create one? [y/n]: " opt
        string match -qr -- '(?i)^y$' $opt; or return 1
        set page $HOME/.tldr/cache/pages/common/$argv.md
        cp (dirname (status -f))/command_page_template.md $page
      end
      if sensible-editor $page
        win "Example page for $argv saved at $page"
        peco --update
      end
    else
      test -e $command_list; or update_command_list
      functions -e update_command_list
      test -n "$argv"
      or cat $command_list | sort | peco --initial-filter Regexp \
      | string match -r '^[^(]+(?= \()' | read -l command
      and cat $command_list | sort \
      | peco --query "$argv" --initial-filter Regexp \
      | string match -r '^[^(]+(?= \()' | read -l command
      test -n "$command"; or return 1
      commandline $command
      string match -r '^[^{]+' (commandline -L)
      commandline -C (math (commandline | string match -r '^[^{]+' | wc -m) -1)
    end

  # Query command history
  else if set --query _flag_history
    test -n "$argv"
    and commandline (history | peco --query "$argv")
    or commandline (history | peco)

  # Query processes to be terminated
  else if set --query _flag_terminate
    ps ax -o pid,time,command | string match -vr "^peco --query $argv" \
    | peco --query "$argv" | awk '{print $1}' | xargs kill 2>/dev/null

  # Query files to be opened
  else if set --query _flag_open
    if not check_open_dependencies
      functions -e (string match -r '[^/]+(?=.fish$)' $auxiliary_functions)
      return 1
    else; functions -e check_open_dependencies; end
    if test -z "$argv"
      err "No argument passed"
      return 1
    else if not test -f "$argv"
      set -l query
      for match in (locate "$argv")
        test -d $match; and continue
        string match -qr '/(\.git|Trash)/' $match; and continue
        set query $query $match
      end
      if test -z "$query"
        err "No file found for pattern |$argv|"
        return 1
      end
      set argv (printf "%s\n" $query \
      | peco --on-cancel error --query "$argv" --select-1)
      or return 1
    end
    for file in $argv
      if file $file | string match -qe HTML
        sensible-browser $file; or xdg-open $file
      else if file $file | string match -qe text
        sensible-editor $file; or xdg-open $file
      else; xdg-open $file; end
    end

  # Show instructions
  else if set --query _flag_help
    cat (dirname (status -f))/instructions; test -z "$argv"

  # If the wrapper function's flags aren't used, pass arguments directly
  else; command peco $argv; end
  functions -e (string match -r '[^/]+(?=.fish$)' $auxiliary_functions)
end
